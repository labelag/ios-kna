//
//  IndexType.m
//  KNA
//
//  Created by Hossam Ghareeb on 4/3/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "IndexType.h"
#import "ImageFile.h"
#import "Member.h"


@implementation IndexType

@dynamic htmlText;
@dynamic indexID;
@dynamic lastModifiedDate;
@dynamic latitiude;
@dynamic longtitude;
@dynamic numberOfPages;
@dynamic section;
@dynamic title;
@dynamic type;
@dynamic year;
@dynamic url;
@dynamic images;
@dynamic members;

@end
