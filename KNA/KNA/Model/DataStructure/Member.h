//
//  Member.h
//  KNA
//
//  Created by Hossam Ghareeb on 4/4/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IndexType;

@interface Member : NSManagedObject

@property (nonatomic, retain) NSString * cv;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * photoUrl;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * memberID;
@property (nonatomic, retain) IndexType *indexType;

@end
