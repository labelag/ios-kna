//
//  DataType.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/5/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ImageFile, Member;

@interface DataType : NSManagedObject

@property (nonatomic, retain) NSNumber * indexID;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSNumber * numberOfPages;
@property (nonatomic, retain) NSString * section;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSSet *images;
@property (nonatomic, retain) NSSet *members;
@end

@interface DataType (CoreDataGeneratedAccessors)

- (void)addImagesObject:(ImageFile *)value;
- (void)removeImagesObject:(ImageFile *)value;
- (void)addImages:(NSSet *)values;
- (void)removeImages:(NSSet *)values;

- (void)addMembersObject:(Member *)value;
- (void)removeMembersObject:(Member *)value;
- (void)addMembers:(NSSet *)values;
- (void)removeMembers:(NSSet *)values;

@end
