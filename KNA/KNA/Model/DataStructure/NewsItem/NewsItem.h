//
//  NewsItem.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/24/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSDate *pubDate;
@property (nonatomic, strong) NSString *newsDescription;
@property (nonatomic, strong) NSString *horizontalImageUrl;
@property (nonatomic, strong) NSString *verticalImageUrl;
@property (nonatomic, assign) BOOL hasPriority;
@end
