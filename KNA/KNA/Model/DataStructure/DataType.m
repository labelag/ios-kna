//
//  DataType.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/5/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "DataType.h"
#import "ImageFile.h"
#import "Member.h"


@implementation DataType

@dynamic indexID;
@dynamic lastModifiedDate;
@dynamic numberOfPages;
@dynamic section;
@dynamic title;
@dynamic type;
@dynamic year;
@dynamic images;
@dynamic members;

@end
