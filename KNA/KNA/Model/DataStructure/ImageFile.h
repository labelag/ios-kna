//
//  ImageFile.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/18/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IndexType;

@interface ImageFile : NSManagedObject

@property (nonatomic, retain) NSString * fileUrl;
@property (nonatomic, retain) IndexType *indexType;

@end
