//
//  ImageFile.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/18/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "ImageFile.h"
#import "IndexType.h"


@implementation ImageFile

@dynamic fileUrl;
@dynamic indexType;

@end
