//
//  IndexType.h
//  KNA
//
//  Created by Hossam Ghareeb on 4/3/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ImageFile, Member;

@interface IndexType : NSManagedObject

@property (nonatomic, retain) NSString * htmlText;
@property (nonatomic, retain) NSNumber * indexID;
@property (nonatomic, retain) NSDate * lastModifiedDate;
@property (nonatomic, retain) NSDecimalNumber * latitiude;
@property (nonatomic, retain) NSDecimalNumber * longtitude;
@property (nonatomic, retain) NSNumber * numberOfPages;
@property (nonatomic, retain) NSString * section;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *images;
@property (nonatomic, retain) NSSet *members;
@end

@interface IndexType (CoreDataGeneratedAccessors)

- (void)addImagesObject:(ImageFile *)value;
- (void)removeImagesObject:(ImageFile *)value;
- (void)addImages:(NSSet *)values;
- (void)removeImages:(NSSet *)values;

- (void)addMembersObject:(Member *)value;
- (void)removeMembersObject:(Member *)value;
- (void)addMembers:(NSSet *)values;
- (void)removeMembers:(NSSet *)values;

@end
