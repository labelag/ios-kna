//
//  Member.m
//  KNA
//
//  Created by Hossam Ghareeb on 4/4/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "Member.h"
#import "IndexType.h"


@implementation Member

@dynamic cv;
@dynamic name;
@dynamic photoUrl;
@dynamic title;
@dynamic memberID;
@dynamic indexType;

@end
