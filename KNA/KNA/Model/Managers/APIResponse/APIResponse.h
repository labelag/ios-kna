//
//  APIResponse.h
//  Monasbat
//
//  Created by Hossam Ghareeb on 8/27/13.
//  Copyright (c) 2013 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
@interface APIResponse : NSObject


@property (nonatomic, strong) id result;
@property (nonatomic, strong) NSError *error;
@end
