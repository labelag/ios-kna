//
//  AbstractAPIManager.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/24/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractAPIManager.h"

@implementation AbstractAPIManager


-(AppDelegate *)appDelegate
{
    if (_appDelegate != nil) {
        return _appDelegate;
    }
    
    _appDelegate = [[UIApplication sharedApplication] delegate];
    return _appDelegate;
}
@end
