//
//  AbstractAPIManager.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/24/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIResponse.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "Helper.h"
typedef void (^CompletionBlock)(APIResponse *response);


@interface AbstractAPIManager : NSObject

@property (nonatomic, strong) AppDelegate *appDelegate;

@end
