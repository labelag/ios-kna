//
//  DataManager.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/2/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractAPIManager.h"
#import "IndexType.h"
#import "ImageFile.h"
#import "Member.h"
@interface DataManager : AbstractAPIManager


-(void)getApplicationDataWithCompletionHandler:(CompletionBlock)completionHanlder;

-(NSArray *)getDataOfSection:(NSString *)section;

-(NSArray *)getDataOfSectionWithSearch:(NSString *)section search:(NSString *)search;
@end
