//
//  DataManager.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/2/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "DataManager.h"
#import "SDImageCache.h"
@implementation DataManager

#define DATE_FORMAT @"yyyy-MM-dd HH:mm:ss.SSS"

-(void)getApplicationDataWithCompletionHandler:(CompletionBlock)completionHanlder
{
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:DATA_URL]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        APIResponse *apiResponse = [[APIResponse alloc] init];
        if (!connectionError) {
            NSError *parseError;
//            NSString *res = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSLog(@"response:%@", res);
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&parseError];
            if (!parseError) {
                [self parseDataArrayWithJsonArray:jsonArray];
                NSArray *localObjects = [self getCachedIndexData];
                apiResponse.result = localObjects;
                
            }
            else
            {
                apiResponse.error = parseError;
            }
        }
        else
        {
            apiResponse.error = connectionError;
        }
        
        completionHanlder(apiResponse);
        
    }];
}

-(NSArray *)parseDataArrayWithJsonArray:(NSArray *)jsonArr
{
    //Remove any saved data in table
    NSArray *localObjects = [self getCachedIndexData];
    
    //Check if data is deleted
    //If every record in DB is not found in the response, delete it
    
    for (IndexType *indexData in localObjects) {
        BOOL found = NO;
        for (NSDictionary *objDic in jsonArr) {
            if ([objDic[@"id"] integerValue] == indexData.indexID.integerValue) {
                found = YES;
                break;
            }
        }
        if (!found) { //The cached record is not found in reponse, so delete it
            [self deleteIndexObject:indexData];
        }
    }
    
    //Now check if every record in reponse is exists in DB, if not , insert it

    for (NSDictionary *objDic in jsonArr) {
        IndexType *indexType = [self getObjectWithIDFromCache:objDic[@"id"]];
        if (indexType == nil) {
            //Insert this object to DB, its new
            [self insertIndexTypeToCoreDataWithDic:objDic];
        }
        else
        {
            //It needs update here
            NSDate *serverDate = [Helper getDateFromString:objDic[@"date"] WithFormat:DATE_FORMAT];
            NSDate *localDate = indexType.lastModifiedDate;
            if ([serverDate compare:localDate] == NSOrderedDescending) {
                //Here we need update
                //Remove local cache if found
                [self deleteIndexObject:indexType];
                [self insertIndexTypeToCoreDataWithDic:objDic];
                
            }
        }
    }
    return nil;

}

-(void)removeSaveDataForIndexType:(IndexType *)indexType
{
    for (ImageFile *imageFile in indexType.images) {
        [[SDImageCache sharedImageCache] removeImageForKey:imageFile.fileUrl fromDisk:YES];
    }
    for (Member *member in indexType.members) {
        [[SDImageCache sharedImageCache] removeImageForKey:member.photoUrl fromDisk:YES];
        //Delete here any saved cv PDF 
    }
    //Remove PDF Folder
    NSString* cachesDirectory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    cachesDirectory = [[[cachesDirectory stringByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]] stringByAppendingPathComponent:@"fsCachedData"] copy];
    
	if([[NSFileManager defaultManager] fileExistsAtPath:cachesDirectory]) {
        [[NSFileManager defaultManager] removeItemAtPath:cachesDirectory error:NULL];
	}
    
    
}

-(void)insertIndexTypeToCoreDataWithDic:(NSDictionary *)objDic
{
    NSManagedObjectContext *context = self.appDelegate.managedObjectContext;
    IndexType *indexType = [NSEntityDescription insertNewObjectForEntityForName:@"IndexType" inManagedObjectContext:context];
    indexType.title = objDic[@"title"];
    indexType.section = [objDic[@"section"] lowercaseString];
    indexType.numberOfPages = objDic[@"number_of_page"];
    indexType.lastModifiedDate = [Helper getDateFromString:objDic[@"date"] WithFormat:DATE_FORMAT];
    indexType.type = objDic[@"type"];
    indexType.indexID = objDic[@"id"];
    int year = [objDic[@"year"] integerValue];
    indexType.year = @(year);
    indexType.url = objDic[@"url"];
    
    NSString *html = objDic[@"file"];
    indexType.htmlText = html;
    
    if ([objDic[@"type"] isEqualToString:@"static"])
    {
        
        
        NSDictionary *mapDic  = objDic[@"map"];
        if (mapDic) {

            indexType.latitiude = [NSDecimalNumber decimalNumberWithString:mapDic[@"latitude"]];
            indexType.longtitude = [NSDecimalNumber decimalNumberWithString:mapDic[@"longtitude"]];
        }
    }
    
    if ([objDic[@"type"] isEqualToString:@"members-list"]) {
        //we will parse array of members objects
        NSArray *members = objDic[@"members"];
        for (NSDictionary *memberObj in members) {
            Member *member = [NSEntityDescription insertNewObjectForEntityForName:@"Member" inManagedObjectContext:context];
            member.cv = memberObj[@"cv"];
            member.photoUrl = memberObj[@"photo"];
            member.name = memberObj[@"name"];
            member.title = memberObj[@"title"];
            id num = memberObj[@"id"];
            member.memberID = num;
            [indexType addMembersObject:member];
        }
    }
    else
    {
        //Parse images array here
        NSArray *images = objDic[@"images"];
        for (NSString *url in images) {
           ImageFile *image = [NSEntityDescription insertNewObjectForEntityForName:@"ImageFile" inManagedObjectContext:context];
            image.fileUrl = url;
            [indexType addImagesObject:image];
        }
    }
    NSError *error;
    [context save:&error];
    
    NSLog(@"Error:%@", error);
    
}

-(IndexType *)getObjectWithIDFromCache:(NSString *)objID
{
    NSManagedObjectContext *context = self.appDelegate.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IndexType" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"indexID == %@", objID];
    [request setPredicate:predicate];
    NSArray *objects =  [context executeFetchRequest:request error:nil];
    if (objects.count) {
        return objects.lastObject;
    }
    return nil;
}

-(void)deleteIndexObject:(IndexType *)indexType
{
    //Any deleted objects will delete any saved image files
    [self removeSaveDataForIndexType:indexType];
    NSManagedObjectContext *context = self.appDelegate.managedObjectContext;

    [context deleteObject:indexType];
    NSError *error;
    [context save:&error];
    
}

-(NSArray *)getCachedIndexData
{
    NSManagedObjectContext *context = self.appDelegate.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IndexType" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *fetechedObjects = [context executeFetchRequest:request error:&error];
    return fetechedObjects;
    
}


-(NSArray *)getDataOfSection:(NSString *)section
{
    NSManagedObjectContext *context = self.appDelegate.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IndexType" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"section == %@", section];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"indexID" ascending:YES];

    [request setPredicate:predicate];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *objects =  [context executeFetchRequest:request error:nil];
    
    return objects;
}

-(NSArray *)getDataOfSectionWithSearch:(NSString *)section search:(NSString *)search
{
        NSManagedObjectContext *context = self.appDelegate.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IndexType" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    if (search.length > 0) {
        NSPredicate *predicate =     [NSPredicate predicateWithFormat:@"(title contains[c] %@) AND (section == %@)", search, section];
        [request setPredicate:predicate];
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"indexID" ascending:YES];
    
    
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *objects =  [context executeFetchRequest:request error:nil];
    
    return objects;
}


@end
