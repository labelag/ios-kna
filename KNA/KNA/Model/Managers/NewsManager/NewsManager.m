//
//  NewsManager.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/24/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "NewsManager.h"

@interface NewsManager()
@property (nonatomic, strong) NSOperationQueue *queue;

@end
@implementation NewsManager



-(void)requestNewsWithCompletionHandler:(CompletionBlock)handler
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:NEWS_RSS_URL]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        //Parse the encoding and change it to utf8 enconding
        NSString *myStr = [[NSString alloc] initWithData:data encoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingWindowsArabic)];
        
        myStr = [myStr stringByReplacingOccurrencesOfString:@"encoding=\"windows-1256\"" withString:@""];
        NSData *aData = [myStr dataUsingEncoding:NSUTF8StringEncoding];
        
        [self fireParsingForData:aData withCompletionHandler:handler];
        
//        NSLog(@"res %@", myStr);
        
    }];
}


-(void)fireParsingForData:(NSData *)data withCompletionHandler:(CompletionBlock)handler
{
    // create the queue to run our ParseOperation
    self.queue = [[NSOperationQueue alloc] init];
    
    // create an ParseOperation (NSOperation subclass) to parse the RSS feed data
    // so that the UI is not blocked
    ParseOperation *parser = [[ParseOperation alloc] initWithData:data];
    
    parser.errorHandler = ^(NSError *parseError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            APIResponse *response =  [[APIResponse alloc] init];
            response.error = parseError;
            handler(response);
        });
    };
    
    // Referencing parser from within its completionBlock would create a retain
    // cycle.
    __weak ParseOperation *weakParser = parser;
    
    parser.completionBlock = ^(void) {
        if (weakParser.newsItemsList) {
            // The completion block may execute on any thread.  Because operations
            // involving the UI are about to be performed, make sure they execute
            // on the main thread.
            dispatch_async(dispatch_get_main_queue(), ^{
                
                APIResponse *response =  [[APIResponse alloc] init];
                response.result = weakParser.newsItemsList;
                handler(response);
            });
        }
        
        // we are finished with the queue and our ParseOperation
        self.queue = nil;
    };
    
    [self.queue addOperation:parser]; // this will start the "ParseOperation"
}

@end
