//
//  NewsManager.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/24/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractAPIManager.h"
#import "ParseOperation.h"
@interface NewsManager : AbstractAPIManager


-(void)requestNewsWithCompletionHandler:(CompletionBlock)handler;
@end
