

#import "ParseOperation.h"
#import "NewsItem.h"

// string contants found in the RSS feed
static NSString *kTitle            = @"title";
static NSString *kLink             = @"link";
static NSString *kPubDate          = @"pubDate";
static NSString *kDescription      = @"description";
static NSString *kHorizontalimage  = @"Horizontalimage";
static NSString *kVerticalimage    = @"Verticalimage";
static NSString *kPriority         = @"priority";
static NSString *kEntryStr         = @"item";


@interface ParseOperation () <NSXMLParserDelegate>
// Redeclare appRecordList so we can modify it.
@property (nonatomic, strong) NSArray *newsItemsList;
@property (nonatomic, strong) NSData *dataToParse;
@property (nonatomic, strong) NSMutableArray *workingArray;
@property (nonatomic, strong) NewsItem *workingEntry;
@property (nonatomic, strong) NSMutableString *workingPropertyString;
@property (nonatomic, strong) NSArray *elementsToParse;
@property (nonatomic, readwrite) BOOL storingCharacterData;
@property (nonatomic, strong) NSDateFormatter *formatter;
@end


@implementation ParseOperation

- (id)initWithData:(NSData *)data
{
    self = [super init];
    if (self != nil)
    {
        _dataToParse = data;
        _elementsToParse = [[NSArray alloc] initWithObjects:kTitle, kLink, kPubDate, kDescription, kHorizontalimage, kVerticalimage, kPriority, nil];
    }
    return self;
}

// -------------------------------------------------------------------------------
//	main
//  Entry point for the operation.
//  Given data to parse, use NSXMLParser and process all news.
// -------------------------------------------------------------------------------
- (void)main
{
    self.formatter = [[NSDateFormatter alloc] init];
    self.formatter.dateFormat = @"yyyy-MM-dd";
    
    self.workingArray = [NSMutableArray array];
    self.workingPropertyString = [NSMutableString string];
    
    // It's also possible to have NSXMLParser download the data, by passing it a URL, but this is not
    // desirable because it gives less control over the network, particularly in responding to
    // connection errors.
    //
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:self.dataToParse];
    [parser setDelegate:self];
    [parser parse];
    
    if (![self isCancelled])
    {
        self.newsItemsList = [NSArray arrayWithArray:self.workingArray];
    }
    
    self.formatter = nil;
    self.workingArray = nil;
    self.workingPropertyString = nil;
    self.dataToParse = nil;
}

#pragma mark - RSS processing

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
                                        namespaceURI:(NSString *)namespaceURI
                                       qualifiedName:(NSString *)qName
                                          attributes:(NSDictionary *)attributeDict
{
    
    if ([elementName isEqualToString:kEntryStr])
	{
        self.workingEntry = [[NewsItem alloc] init];
    }
    self.storingCharacterData = [self.elementsToParse containsObject:elementName];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
                                      namespaceURI:(NSString *)namespaceURI
                                     qualifiedName:(NSString *)qName
{
    
    if (self.workingEntry)
	{
        if (![elementName isEqualToString:kEntryStr])
        {
            NSString *trimmedString = [self.workingPropertyString stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [self.workingPropertyString setString:@""];  // clear the string for next time
            
            if ([elementName isEqualToString:kTitle])
            {
                self.workingEntry.title = trimmedString;
            }
            else if ([elementName isEqualToString:kLink])
            {        
                self.workingEntry.link = trimmedString;
            }
            else if ([elementName isEqualToString:kPubDate])
            {
                self.workingEntry.pubDate = [self.formatter dateFromString:trimmedString];
            }
            else if ([elementName isEqualToString:kDescription])
            {
                self.workingEntry.newsDescription = trimmedString;
            }
            else if ([elementName isEqualToString:kHorizontalimage])
            {
                self.workingEntry.horizontalImageUrl = trimmedString;
            }
            else if ([elementName isEqualToString:kVerticalimage])
            {
                self.workingEntry.verticalImageUrl = trimmedString;
            }
            else if ([elementName isEqualToString:kPriority])
            {
                trimmedString = [trimmedString lowercaseString];
                self.workingEntry.hasPriority = [trimmedString isEqualToString:@"yes"]? YES : NO;
            }
        }
        else if ([elementName isEqualToString:kEntryStr])
        {
            [self.workingArray addObject:self.workingEntry];  
            self.workingEntry = nil;
        }
    }
    
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (self.storingCharacterData)
    {
        [self.workingPropertyString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    if (self.errorHandler)
        self.errorHandler(parseError);
}

@end
