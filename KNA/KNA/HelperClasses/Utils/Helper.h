//
//  Helper.h
//  KNA
//
//  Created by Hossam Ghareeb on 8/30/13.
//  Copyright (c) 2013 Hossam Ghareeb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject

+(NSDate *)getDateFromString:(NSString *)dateString WithFormat:(NSString *)format;
+(NSString *)getStringFromDate:(NSDate *)date withFormat:(NSString *)format;
+(NSString *)getDocumentsPath;
+(NSString *)getCachesPath;
+(NSString *)getParseFileCachesPath;
+(BOOL)isTextIsArabic:(NSString *)text;
+(void)printCurrentFonts;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
@end
