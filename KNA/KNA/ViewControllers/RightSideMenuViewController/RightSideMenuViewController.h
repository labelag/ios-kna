//
//  RightSideMenuViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/16/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "UIExpandableTableView.h"
#import "RightMenuCell.h"
#import "RightMenuNormalCell.h"
#import "CustomTabBarViewController.h"
@interface RightSideMenuViewController : AbstractViewController <UIExpandableTableViewDelegate, UIExpandableTableViewDatasource, UITextFieldDelegate>
{
    UIExpansionStyle currentStyle;
}

@property (nonatomic, weak) CustomTabBarViewController *tabBarController;
@end
