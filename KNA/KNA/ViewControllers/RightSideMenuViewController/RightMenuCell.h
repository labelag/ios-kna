//
//  RightMenuCell.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/17/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "GHCollapsingAndSpinningTableViewCell.h"
#import "Constants.h"
@interface RightMenuCell : GHCollapsingAndSpinningTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

-(void)bindData:(NSString *)title section:(int)section;
@end
