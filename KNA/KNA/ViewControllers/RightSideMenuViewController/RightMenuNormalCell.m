//
//  RightMenuNormalCell.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/17/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "RightMenuNormalCell.h"

@implementation RightMenuNormalCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self  = [super initWithCoder:aDecoder]) {
        UIImageView *bgView = [[UIImageView alloc]initWithFrame:self.frame];
        bgView.backgroundColor = [UIColor colorWithRed:22.0 / 255.0 green:163.0 / 255.0 blue:171.0 / 255.0 alpha:1];
        self.selectedBackgroundView = bgView;
    }
    
    return self;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.cellLabelTitle.font = [UIFont fontWithName:NADEEM_FONT size:16];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
