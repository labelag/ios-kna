//
//  RightMenuNormalCell.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/17/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
@interface RightMenuNormalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabelTitle;

@end
