//
//  RightMenuCell.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/17/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "RightMenuCell.h"

@implementation RightMenuCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self  = [super initWithCoder:aDecoder]) {
        UIImageView *bgView = [[UIImageView alloc]initWithFrame:self.frame];
        bgView.backgroundColor = [UIColor colorWithRed:22.0 / 255.0 green:163.0 / 255.0 blue:171.0 / 255.0 alpha:1];
        self.selectedBackgroundView = bgView;
    }
    
    return self;
}

-(void)bindData:(NSString *)title section:(int)section
{
    

        
    self.cellLabelTitle.font = [UIFont fontWithName:NADEEM_FONT size:16];
    
    self.cellLabelTitle.text = title;
    NSString *imageName;
    switch (section) {
        case 0: //News Section
            imageName = @"newsIcon.png";
            break;
        case 1: case 2: case 3: //Docs, Constitution, Memo section
            imageName = @"docsIcon.png";
            break;
        case 4: // Members
            imageName = @"staffIcon.png";
            break;
        case 5: //Albums section
            imageName = @"albumsIcon.png";
            break;
        case 6: //About
            imageName = @"aboutIcon.png";
            break;
            
        default:
            break;
    }
    
    self.iconImageView.image = [UIImage imageNamed:imageName];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
