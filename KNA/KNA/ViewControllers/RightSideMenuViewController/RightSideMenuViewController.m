//
//  RightSideMenuViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/16/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "RightSideMenuViewController.h"
#import "DataManager.h"

@interface RightSideMenuViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIExpandableTableView *rightMenuTableView;

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSMutableArray *rowsContents;

@end

#define SECTION_COUNT 5

@implementation RightSideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self setupSearchTextView];
    
    self.rightMenuTableView.delegate = self;
    self.rightMenuTableView.dataSource = self;
    
    self.rightMenuTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightMenuBg.png"]];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"]];

    self.titles = dictionary[@"MenuTitles"];
    
    self.rowsContents = [NSMutableArray array];
    for (int i = 0; i < self.titles.count; i++) {
        [self.rowsContents addObject:@[]];
    }
    
    currentStyle = UIExpansionStyleCollapsed;
}

-(void)receiveIndexFileNotification:(NSNotification *)notification
{
    [self requestDataFromDB];
}

-(void)requestDataFromDB
{
    NSArray *newsArr = @[];
    NSArray *constitutionArr = @[];
    NSArray *memoArr = @[];
    NSArray *docsArr = @[];
    NSArray *memebersArr = @[];
    NSArray *albums = @[];
    NSArray *about = @[];
    
    DataManager *manager = [[DataManager alloc] init];
    docsArr = [manager getDataOfSection:DOCUMENTS_SECTION];
    memebersArr = [manager getDataOfSection:MEMBERS_SECTION];
    about = [manager getDataOfSection:ABOUT_SECTION];
    constitutionArr = [manager getDataOfSection:CONSTITUTION_SECTION];
    memoArr = [manager getDataOfSection:MEMO_SECTION];
    self.rowsContents = [NSMutableArray arrayWithObjects:newsArr, constitutionArr, memoArr, docsArr, memebersArr, albums, about, nil];

    [self.rightMenuTableView reloadData];
}
-(void)requestDataFromDBWithSearch:(NSString *)search
{
    NSArray *newsArr = @[];
    NSArray *constitutionArr = @[];
    NSArray *memoArr = @[];
    NSArray *docsArr = @[];
    NSArray *memebersArr = @[];
    NSArray *albums = @[];
    NSArray *about = @[];
    
    DataManager *manager = [[DataManager alloc] init];
    docsArr = [manager getDataOfSectionWithSearch:DOCUMENTS_SECTION search:search];
    memebersArr = [manager getDataOfSectionWithSearch:MEMBERS_SECTION search:search];
    about = [manager getDataOfSectionWithSearch:ABOUT_SECTION search:search];
    constitutionArr = [manager getDataOfSectionWithSearch:CONSTITUTION_SECTION search:search];
    memoArr = [manager getDataOfSectionWithSearch:MEMO_SECTION search:search];

    
    self.rowsContents = [NSMutableArray arrayWithObjects:newsArr, constitutionArr, memoArr, docsArr, memebersArr, albums, about, nil];
    [self.rightMenuTableView reloadData];
    
    for (int i = 0; i < self.titles.count && search.length > 0; i++) {
        [self.rightMenuTableView expandSection:i animated:NO];
    }
}

-(void)setupSearchTextView
{
    //Customize the search field
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchIcon.png"]];
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    self.searchTextField.leftView = img;
    self.searchTextField.delegate = self;
    
    UIColor *color = [UIColor colorWithRed:115.0 / 255.0 green:118.0 / 255.0 blue:116.0 / 255.0 alpha:1];
    self.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"البحث" attributes:@{NSForegroundColorAttributeName: color}];
}

#pragma mark - TextFieldDelegate -
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSLog(@"%@, %@, %d, %d", textField.text, string, range.location, range.length);
    NSString *search;
    if(range.length == 0)
    {
        search = [NSString stringWithFormat:@"%@%@", textField.text, string];
    }
    else
    {
        search = [textField.text substringToIndex:textField.text.length - range.length];
    }
    NSLog(@"Search:%@", search);
    [self requestDataFromDBWithSearch:search];
    return YES;
}

#pragma mark - UIExpandableTableViewDatasource -

- (BOOL)tableView:(UIExpandableTableView *)tableView canExpandSection:(NSInteger)section {
    // return YES, if the section should be expandable
    return section == 1 || section == 2 || section == 3 || section == 4 || section == 6;
}

- (BOOL)tableView:(UIExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
    // return YES, if you need to download data to expand this section. tableView will call tableView:downloadDataForExpandableSection: for this section
    return NO;
}

- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(UIExpandableTableView *)tableView expandingCellForSection:(NSInteger)section {
    
    RightMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"section"];
    
    [cell bindData:self.titles[section] section:section];
    cell.accessoryView.hidden = NO;
    return cell;
}

#pragma mark - UIExpandableTableViewDelegate

- (void)tableView:(UIExpandableTableView *)tableView downloadDataForExpandableSection:(NSInteger)section {
    // download your data here
}

- (void)tableView:(UIExpandableTableView *)tableView willExpandSection:(NSUInteger)section animated:(BOOL)animated
{
    NSLog(@"%@: %lu", NSStringFromSelector(_cmd), (unsigned long)section);
//    [self.view endEditing:YES];
}

- (void)tableView:(UIExpandableTableView *)tableView didExpandSection:(NSUInteger)section animated:(BOOL)animated
{
    NSLog(@"%@: %lu", NSStringFromSelector(_cmd), (unsigned long)section);
}

- (void)tableView:(UIExpandableTableView *)tableView willCollapseSection:(NSUInteger)section animated:(BOOL)animated
{
    NSLog(@"%@: %lu", NSStringFromSelector(_cmd), (unsigned long)section);
//    [self.view endEditing:YES];
}

- (void)tableView:(UIExpandableTableView *)tableView didCollapseSection:(NSUInteger)section animated:(BOOL)animated
{
    NSLog(@"%@: %lu", NSStringFromSelector(_cmd), (unsigned long)section);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return self.titles.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 32.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSArray *sectionContents = self.rowsContents[section];
    return sectionContents.count + 1;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    
    RightMenuNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // Configure the cell...
    NSString *title;
    if (indexPath.row == 0) {
        RightMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"section"];
        [cell bindData:self.titles[indexPath.section] section:indexPath.section];
        cell.accessoryView.hidden = YES;
        return cell;
    }
    else
    {
        NSArray *rows = self.rowsContents[indexPath.section];
        IndexType *type = rows[indexPath.row - 1];
        
        title = type.title;
    }
    
    cell.cellLabelTitle.text = title;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"selected %d, %d", indexPath.section, indexPath.row);
    NSArray *rows = self.rowsContents[indexPath.section];
    NSString *title;
    if (rows.count == 0) {
        title = @"";
    }
    else
    {
        IndexType *type = rows[indexPath.row - 1];
        title = type.title;
    }
    int section = indexPath.section;
    switch (indexPath.section) {
        case 0:
            section = 0;
            break;
        case 1: case 2: case 3:
            section = 1;
            break;
        case 4:
            section = 2;
            break;
        case 5:
            section = 3;
            break;
        case 6:
            section = 4;
            break;
            
        default:
            break;
    }

    [self.tabBarController updateSelectionBySection:section withTitle:title];
    [self.masterDetailViewController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
