//
//  StaticPageViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/18/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "StaticPageViewController.h"

@interface StaticPageViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *staticMapView;
@property (weak, nonatomic) IBOutlet UIImageView *divider;
@property (weak, nonatomic) IBOutlet UIWebView *staticWebView;
@end

@implementation StaticPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    [self.staticWebView setBackgroundColor:[UIColor greenColor]];
    [self.staticWebView setOpaque:NO];
    NSString *fileUrl = self.selectedIndex.htmlText;
    [self.staticWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.selectedIndex.htmlText]]];
   
    [self displayLocation];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.divider.isHidden) {
        CGRect frame =  self.staticWebView.frame;
        CGSize size = frame.size;
        size.height -= self.divider.frame.origin.y;
        size.height += 60;
        frame.size = size;
        
        self.staticWebView.frame = frame;
    }
}

-(void)assignNavBarViews
{
    [super assignNavBarViews];
    if([self.selectedIndex.section isEqualToString:MEMO_SECTION] || [self.selectedIndex.section isEqualToString:CONSTITUTION_SECTION])
    {
        //NO back button here
        self.navigationItem.hidesBackButton = YES;
    }
    else
    {
       [self addDefaultBackButton];
    }
    
}

-(void)displayLocation
{
    NSDecimalNumber *latitude = self.selectedIndex.latitiude;
    NSDecimalNumber *longtitude = self.selectedIndex.longtitude;
    float lat = latitude.floatValue;
    float lon = longtitude.floatValue;
    if (lat == 0 || lon == 0) {
        self.staticMapView.hidden = YES;
        self.divider.hidden = YES;
    }
    else
    {
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(lat, lon);
        
        MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
        MKCoordinateRegion region = {coord, span};
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:coord];
        
        [self.staticMapView setRegion:region];
        [self.staticMapView addAnnotation:annotation];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
