//
//  StaticPageViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/18/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "IndexType.h"
#import <MapKit/MapKit.h>
#import <AddressBook/AddressBook.h>

@interface StaticPageViewController : AbstractViewController


@property (nonatomic, strong) IndexType *selectedIndex;
@end
