//
//  PhotoBrowserViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/7/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "IndexType.h"
#import "ImageFile.h"
#import "Member.h"
#import "MWPhotoBrowser.h"
#import "Constants.h"
@interface PhotoBrowserViewController : MWPhotoBrowser


@property (nonatomic, strong) NSString *albumTitle;
@property (nonatomic, assign) int year;
@property (nonatomic, assign) BOOL isAlbums;
@property (nonatomic, strong) UILabel *coverLabel;

@end
