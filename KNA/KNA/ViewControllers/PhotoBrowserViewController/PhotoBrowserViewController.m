//
//  PhotoBrowserViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/7/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "PhotoBrowserViewController.h"

@interface PhotoBrowserViewController ()


@property (nonatomic, strong) NSMutableArray *photos;


@end

@implementation PhotoBrowserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    
    [super viewDidLoad];
    
    if(self.isAlbums)
        self.hidesBottomBarWhenPushed = NO;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 56, 22);
    [button addTarget:self action:@selector(backViewController:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    if(self.isAlbums)
    {
//        self.navigationItem.hidesBackButton = YES;
        
//        self.navigationItem.leftBarButtonItem = nil;
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(00, 100, 320, 100)];
//        label.text = self.albumTitle;
//        label.numberOfLines = 0;
//        label.lineBreakMode = NSLineBreakByWordWrapping;
//        label.textAlignment = NSTextAlignmentCenter;
//        label.font = [UIFont fontWithName:GEDINAR_FONT_MEDIUM size:20];
//        label.textColor = [UIColor whiteColor];
//        label.center = self.view.center;
        self.coverLabel = nil;
//        [self.view addSubview:self.coverLabel];

    }
    
}

-(void)backViewController:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setNavBarAppearance:(BOOL)animated
{
    //do nothing
    //To draw the original navigation bar style
}

-(void)updateNavigation
{
    //Do nothing, so not to write the title of current page
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
