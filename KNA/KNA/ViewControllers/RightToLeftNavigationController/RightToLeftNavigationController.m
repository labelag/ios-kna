//
//  RightToLeftNavigationController.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/7/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "RightToLeftNavigationController.h"

@interface RightToLeftNavigationController ()

@end

@implementation RightToLeftNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)pushViewControllerWithoutAnimation:(UIViewController *)vc
{
    [super pushViewController:vc animated:NO];
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self prepareCustomAnimationForPush];
    [super pushViewController:viewController animated:NO];
}

-(UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    [self prepareCustomAnimationForPop];
    return [super popViewControllerAnimated:animated];
}


-(void)prepareCustomAnimationForPop
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.45;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [transition setType:kCATransitionPush];
    transition.subtype = kCATransitionFromRight;
    transition.delegate = self;
    [self.view.layer addAnimation:transition forKey:nil];
    
}

-(void)prepareCustomAnimationForPush
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.45;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [transition setType:kCATransitionPush];
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    [self.view.layer addAnimation:transition forKey:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
