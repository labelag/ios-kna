//
//  RightToLeftNavigationController.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/7/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightToLeftNavigationController : UINavigationController


-(void)pushViewControllerWithoutAnimation:(UIViewController *)vc;
@end
