//
//  AbstractDetailsViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/7/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractDetailsViewController.h"

@interface AbstractDetailsViewController ()

@property (nonatomic, strong) NSMutableArray *photos;

@end

@implementation AbstractDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)openItemAtRow:(NSNumber *)row
{
    IndexType *indexType = self.titles[row.integerValue];
    [self startPhotoBrowserWithIndexData:indexType];
}




-(void)startPhotoBrowserWithIndexData:(IndexType *)indexData
{
    self.photos = [NSMutableArray array];
    
    NSArray *images = [indexData.images allObjects];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"fileUrl" ascending:YES];
    images = [images sortedArrayUsingDescriptors:@[sort]];

    for (ImageFile *imgFile in images) {
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:imgFile.fileUrl]]];
        //        [[SDImageCache sharedImageCache] removeImageForKey:imgFile.fileUrl fromDisk:YES];
        
    }
    
    
    
    if ([indexData.section isEqualToString :@"albums" ]) {
        NSString *string = [NSString stringWithFormat:@"%@ \n %@", indexData.title, indexData.year.stringValue];
        isAlbums = YES;
        [self openPhotoBrowserWithTitle:string];
    }
    else
    {
        [self openPhotoBrowserWithTitle:@""];
    }
    
}


-(void)openPhotoBrowserWithTitle:(NSString *)title
{
    
    PhotoBrowserViewController *browser = [[PhotoBrowserViewController alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = NO; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = YES; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    browser.albumTitle = title;
    if(isAlbums)
    {
        browser.hidesBottomBarWhenPushed = NO;
        browser.isAlbums = isAlbums;
        [self.navigationController pushViewController:browser animated:NO];
    }
    else
    {
        [self.navigationController pushViewController:browser animated:NO];
    }
    // Present
//    [self.navigationController pushViewController:browser animated:NO];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowser Delegate -
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return self.photos.count;
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index
{
    if(isAlbums)
    {
        PhotoBrowserViewController *browser = (PhotoBrowserViewController *)photoBrowser;
        browser.coverLabel.hidden = index != 0;
    }
    NSLog(@"%d", index);
}
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{

    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
