//
//  AbstractDetailsViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/7/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//


//This class is used to take the selected IndexType object and open it's images in a photobrowser

#import "AbstractViewController.h"
#import "PhotoBrowserViewController.h"
#import "RightToLeftNavigationController.h"
@interface AbstractDetailsViewController : AbstractViewController <MWPhotoBrowserDelegate>
{
    BOOL isAlbums;
}

@property (nonatomic, strong) NSArray *titles;

-(void)startPhotoBrowserWithIndexData:(IndexType *)indexData;

-(void)openItemAtRow:(NSNumber *)row;
-(void)openItemWithTitle:(NSString *)title;
@end
