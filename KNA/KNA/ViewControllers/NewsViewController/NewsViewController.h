//
//  NewsViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "NewsManager.h"
#import "NewsItem.h"
#import "PageContentViewController.h"
@interface NewsViewController : AbstractViewController <UITableViewDataSource, UITableViewDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate, PageContentViewControllerDelegate>

@end
