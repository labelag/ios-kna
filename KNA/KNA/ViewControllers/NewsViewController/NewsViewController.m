//
//  NewsViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsCell.h"
#import "NewsDetailsViewController.h"
#import "PageContentViewController.h"
@interface NewsViewController ()

@property (weak, nonatomic) IBOutlet UIPageControl *pageController; //I use this instead of the built in one in UIPageViewController

@property (nonatomic, strong) NSMutableArray *newsItems;
@property (nonatomic, strong) NSMutableArray *priorityNewsItems;
@property (weak, nonatomic) IBOutlet UITableView *newsTableView;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, weak) UIPageViewController *pageViewController;

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.newsItems = [NSMutableArray array];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"EEEE, dd MMM, yyyy";
    
    for (id vc in self.childViewControllers) {
        if ([vc isKindOfClass:[UIPageViewController class]]) {
            self.pageViewController = vc;
            self.pageViewController.dataSource = nil;
        }
    }
	
//    [self requestRSSNews];

}

-(void)receiveIndexFileNotification:(NSNotification *)notification
{
    APIResponse *response = notification.userInfo[INDEX_FILE_RESPONSE_KEY];
    if (response.error) {
        [self showWarningMsg:response.error.userInfo[@"error"] inView:self.view];
    }
    else
    {
        [self requestRSSNews];
    }
}


-(void)requestRSSNews
{

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading News!";
    NewsManager *manager = [[NewsManager alloc] init];
    [manager requestNewsWithCompletionHandler:^(APIResponse *response) {
        
        [hud hide:YES];
        
        if (!response.error) {
            self.newsItems = [NSMutableArray arrayWithArray:response.result];
            self.priorityNewsItems = [NSMutableArray array];
            [self didLoadNewsItems];
            
            [self.newsTableView reloadData];
        }
        else
        {
            [self showWarningMsg:[NSString stringWithFormat:@"Error %@",response.error.userInfo[@"error"]] inView:self.view];
        }
        
    }];
}

-(void)didLoadNewsItems
{
    NSMutableArray *deletedObjects = [NSMutableArray array];
    for (NewsItem *item in self.newsItems) {
        if (self.priorityNewsItems.count == 5) {
            break;
        }
        NSRange range = [item.title rangeOfString:@"RSS Feed"];
        
        if (range.location != NSNotFound) {
            [deletedObjects addObject:item];
        }
        else
        {
            if (item.hasPriority) {
                [self.priorityNewsItems addObject:item];
            }
        }
        
        
    }
    
    [self.newsItems removeObjectsInArray:deletedObjects];
    //NO need to delete the priotiry news 
    
//    for (NewsItem *item in self.priorityNewsItems) {
//        [self.newsItems removeObject:item];
//    }
    
    self.pageController.numberOfPages = self.priorityNewsItems.count;
    self.pageController.currentPage = 0;
    
    self.pageViewController.dataSource = self;

    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    //Hide the built in pagecontroller
    
    NSArray *subviews = self.pageViewController.view.subviews;
    UIPageControl *thisControl = nil;
    for (int i=0; i<[subviews count]; i++) {
        if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {
            thisControl = (UIPageControl *)[subviews objectAtIndex:i];
        }
    }
    
    thisControl.hidden = true;
    CGRect frame = self.pageViewController.view.frame;
    frame.size.height += 40;
    self.pageViewController.view.frame = frame;
    
    
    // Change the size of page view controller
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.newsItems.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NewsItem *item = self.newsItems[indexPath.row] ;
    [cell bindCellData:item formattedDate:[self.dateFormatter stringFromDate:item.pubDate]];
//    NSLog(@"=======================");
//    NSLog(@"%@, %@, %@", item.link, item.horizontalImageUrl, item.verticalImageUrl);
//    NSLog(@"=======================");
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self newsClickedAtIndex:indexPath.row isPriority:NO];
}

-(void)newsClickedAtIndex:(int)index isPriority:(BOOL)isPriority
{
    NewsDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_NEWS_DETAILS];
    detailsVC.selectedItem = isPriority? self.priorityNewsItems[index] : self.newsItems[index];
    detailsVC.masterDetailViewController = self.masterDetailViewController;
    [self.navigationController pushViewController:detailsVC animated:NO];
}



#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    self.pageController.currentPage = index;
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    self.pageController.currentPage = index;
    index++;
    if (index == [self.priorityNewsItems count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.priorityNewsItems count] == 0) || (index >= [self.priorityNewsItems count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_PAGE_CONTENT_VC];
    pageContentViewController.delegate = self;
    
    NewsItem *item  = self.priorityNewsItems[index];
    pageContentViewController.imageFile = item.horizontalImageUrl;
    pageContentViewController.titleText = item.title;
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

-(void)didClickedPageAtIndex:(int)index
{
    [self newsClickedAtIndex:index isPriority:YES];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.priorityNewsItems count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
