//
//  NewsCell.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/25/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)bindCellData:(NewsItem *)item formattedDate:(NSString *)date;
{
    
    [self.newsImageView setImageWithURL:[NSURL URLWithString:item.verticalImageUrl] placeholderImage:[UIImage imageNamed:@"defaultVerticalImg.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    
    self.newsTitleLabel.text = item.title;
    self.newsTitleLabel.font = [UIFont fontWithName:NADEEM_FONT size:18];
    self.newsDateLabel.text = date;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
