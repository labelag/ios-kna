//
//  NewsCell.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/25/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsItem.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
@interface NewsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *newsDateLabel;

-(void)bindCellData:(NewsItem *)item formattedDate:(NSString *)date;
@end
