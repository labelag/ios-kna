//
//  AlbumsCell.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/18/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AlbumsCell.h"

@implementation AlbumsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)bindIndexData:(IndexType *)indexType
{
    NSArray *images = [indexType.images allObjects];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"fileUrl" ascending:YES];
    images = [images sortedArrayUsingDescriptors:@[sort]];
    NSString *url = @"";
    if (images.count) {
        ImageFile *file = images[0];
        url = file.fileUrl;
    }
    [self.coverImageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"defaultVerticalImg.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    UIFont *font = [UIFont fontWithName:GEDINAR_FONT_MEDIUM size:18];
    self.albumTitleLabel.font = font;
    self.albumDateLabel.font = font;
    
    self.albumTitleLabel.text = indexType.title;
    self.albumDateLabel.text = indexType.year.stringValue;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
