//
//  AlbumsCell.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/18/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndexType.h"
#import "ImageFile.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "Constants.h"
@interface AlbumsCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *albumTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *albumDateLabel;

-(void)bindIndexData:(IndexType *)indexType;

@end
