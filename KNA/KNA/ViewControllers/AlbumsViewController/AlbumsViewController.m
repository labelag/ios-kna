//
//  AlbumsViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AlbumsViewController.h"

@interface AlbumsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *albumsTableView;
@end

@implementation AlbumsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.albumsTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboutTableBg.png"]];
    
    DataManager *manager = [[DataManager alloc] init];
    self.titles = [manager getDataOfSection:@"albums"];
//    [self openItemAtRow:0];
}


#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titles.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlbumsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"albumCell"];
    IndexType *type = self.titles[indexPath.row];

    [cell bindIndexData:type];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 175.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self openItemAtRow:@(indexPath.row)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
