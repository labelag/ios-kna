//
//  AbstractViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 1/23/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Helper.h"
#import "MasterDetailsViewController.h"
#import "Constants.h"
@interface AbstractViewController : UIViewController



@property (nonatomic, weak) MasterDetailsViewController *masterDetailViewController;
@property (nonatomic, strong) UIView *rightView;
@property (nonatomic, strong) UIView *leftView;


-(IBAction)goBack:(id)sender;
-(IBAction)toogleMasterView:(id)sender;

-(void)receiveIndexFileNotification:(NSNotification *) notification;

//This is used by subclasses to assign any custom views to be added in nav bar
//Default behavior now: it create right button to toogle right menu
-(void)assignNavBarViews;

-(void)addDefaultBackButton;

-(void)showWarningMsg:(NSString *)warnningMsg inView:(UIView *)view;
-(void)showLoadingMsg:(NSString *)loadingMsg inView:(UIView *)view;
-(void)hideHUDInView:(UIView *)view;
@end
