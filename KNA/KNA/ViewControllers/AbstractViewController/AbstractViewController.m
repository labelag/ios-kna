//
//  AbstractViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 1/23/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"

@interface AbstractViewController ()

@end

@implementation AbstractViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
	// Do any additional setup after loading the view.
    
    
    [self assignNavBarViews];
    [self setupNavBar];
    
    //listen to notification of index file download
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveIndexFileNotification:)
                                                 name:INDEX_FILE_NOTIFICATION
                                               object:nil];
}

-(void)addDefaultBackButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 56, 22);
    [button addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    self.leftView = button;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



-(void)assignNavBarViews
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 35, 35);
    [button setImage:[UIImage imageNamed:@"openMenuButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(toogleMasterView:) forControlEvents:UIControlEventTouchUpInside];
    self.rightView = button;
}

-(void)receiveIndexFileNotification:(NSNotification *) notification
{
    
}

-(void)setupNavBar
{
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightView];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftView];
}

-(void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)toogleMasterView:(id)sender
{
    [self.masterDetailViewController openDrawerSide:MMDrawerSideRight animated:YES completion:^(BOOL finished) {
        [self.view endEditing:YES];
    }];
}
-(void)showLoadingMsg:(NSString *)loadingMsg inView:(UIView *)view
{
    
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = loadingMsg;
}
-(void)hideHUDInView:(UIView *)view
{
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}
-(void)showWarningMsg:(NSString *)warnningMsg inView:(UIView *)view;
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
	
	// Configure for text only and offset down
	hud.mode = MBProgressHUDModeText;
//	hud.labelText = warnningMsg;
    hud.detailsLabelText = warnningMsg;
	hud.margin = 10.f;
	hud.yOffset = 150.f;
	hud.removeFromSuperViewOnHide = YES;
	
	[hud hide:YES afterDelay:4];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
