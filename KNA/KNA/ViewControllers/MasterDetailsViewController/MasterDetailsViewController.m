//
//  MasterDetailsViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/16/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "MasterDetailsViewController.h"
#import "AbstractViewController.h"
#import "CustomTabBarViewController.h"
#import "RightSideMenuViewController.h"
@interface MasterDetailsViewController ()

@end

@implementation MasterDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.showsShadow = NO;
    
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    RightSideMenuViewController *rightMenu = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_RIGHT_MENU];
    CustomTabBarViewController *tabbarViewController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_TABBAR_VC];
    rightMenu.masterDetailViewController = self;
    rightMenu.tabBarController = tabbarViewController;
    tabbarViewController.masterDetailViewController = self;
    self.rightDrawerViewController = rightMenu;
    self.centerViewController = tabbarViewController;
    self.leftDrawerViewController = nil;
    
    self.maximumRightDrawerWidth = 237;
    
    [self setGestureCompletionBlock:^(MMDrawerController *drawerController, UIGestureRecognizer *gesture) {
        [drawerController.view endEditing:YES];
    }];
    
	// Do any additional setup after loading the view.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
