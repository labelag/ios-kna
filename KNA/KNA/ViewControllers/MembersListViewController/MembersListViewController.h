//
//  MembersListViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "AboutAssemblyCell.h"
#import "IndexFileOpenerViewController.h"
#import "DataManager.h"
#import "MembersGroupViewController.h"
@interface MembersListViewController : IndexFileOpenerViewController <UITableViewDelegate, UITableViewDataSource>
{
    int currentIndex;
}
@end
