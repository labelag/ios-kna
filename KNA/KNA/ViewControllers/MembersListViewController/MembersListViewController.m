//
//  MembersListViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "MembersListViewController.h"
@interface MembersListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *membersGroupsTableView;


@end

@implementation MembersListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.membersGroupsTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboutTableBg.png"]];
    
    DataManager *manager = [[DataManager alloc] init];
    
    self.titles = [manager getDataOfSection:@"members"];
}


#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    
    return self.titles.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AboutAssemblyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    IndexType *type = self.titles[indexPath.row];
    cell.cellLabel.text = type.title;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self openIndexFileAtIndex:indexPath.row];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
