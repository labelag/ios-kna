//
//  CustomTabBarViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/16/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterDetailsViewController.h"
#import "DataManager.h"
#import "Constants.h"
typedef enum {
    AboutUs = 0,
    Albums,
    Staff,
    Documents,
    News
}TabBarButtonType;
@interface CustomTabBarViewController : UITabBarController





@property (nonatomic, weak) MasterDetailsViewController *masterDetailViewController;


-(void)updateSelectionBySection:(int)section withTitle:(NSString *)title;
@end
