//
//  CustomTabBarViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/16/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "CustomTabBarViewController.h"
#import "AbstractViewController.h"
#import "IndexFileOpenerViewController.h"
@interface CustomTabBarViewController ()

@property (nonatomic, strong) NSMutableArray *tabBarButtons;

@end

@implementation CustomTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Assign the masterDetailviewcontroller for ViewControllers
    
    for (UINavigationController *navC in self.viewControllers) {
        ((AbstractViewController *)navC.viewControllers[0]).masterDetailViewController = self.masterDetailViewController;
    }
    
    [self downloadIndexFile];
    
    [self customizingTabBar];
}


-(void)downloadIndexFile
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Checking for updates!";
    DataManager *manager = [[DataManager alloc] init];
    [manager getApplicationDataWithCompletionHandler:^(APIResponse *response) {
        
        [hud hide:YES];
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:response forKey:INDEX_FILE_RESPONSE_KEY];

        [[NSNotificationCenter defaultCenter] postNotificationName: INDEX_FILE_NOTIFICATION object:nil userInfo:userInfo];

    }];

}

#define BUTTON_HEIGHT 49

-(void)customizingTabBar
{
    self.tabBarButtons = [NSMutableArray array];
    NSArray *barImages = @[@"tabBarAboutIcon.png", @"tabBarAlbumsIcon.png", @"tabBarStaffIcon.png", @"tabBarDocIcon.png", @"tabBarNewsIcon.png"];
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tabBarBg.png"]];
    
    [self.tabBar addSubview:bgImageView];
    
    //Add buttons
    
    float widths[5];
    widths[0] = 66;
    widths[1] = 63;
    widths[2] = 63;
    widths[3] = 62;
    widths[4] = 66;
    
    CGRect frame;
    frame.origin = CGPointZero;
    float currentX = 0;
    for (int i = 0; i < 5; i ++) {
        frame.origin.x = currentX;
        float width = widths[i];
        currentX += width;
        frame.size = CGSizeMake(width, BUTTON_HEIGHT);
        UIImage *img = [UIImage imageNamed:barImages[i]];
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        imgView.contentMode = UIViewContentModeCenter;
        imgView.frame = frame;
        [self.tabBar addSubview:imgView];

        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:img forState:UIControlStateNormal];
        button.tag = i;
        [button addTarget:self action:@selector(tabBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = frame;
        [self.tabBar addSubview:button];
        [self.tabBarButtons addObject:button];
    }
    [self doHighlight:self.tabBarButtons.lastObject];
    self.selectedIndex = News;
}

-(void)tabBarButtonClicked:(UIButton *)clickedButton
{
    self.selectedIndex = clickedButton.tag;
    
    if (clickedButton.tag == 3) { //Documents tab
        UINavigationController *navController = self.viewControllers[3];
        [navController popToRootViewControllerAnimated:NO];
    }
    
    [self performSelector:@selector(doHighlight:) withObject:clickedButton afterDelay:0];

    
    for (UIButton *tabBarButton in self.tabBarButtons) {
        if (tabBarButton.tag != clickedButton.tag) {
            [tabBarButton setHighlighted:NO];
            [tabBarButton setBackgroundColor:[UIColor clearColor]];
        }
    }
}
- (void)doHighlight:(UIButton*)b {
    [b setHighlighted:YES];
    [b setBackgroundColor:[UIColor colorWithRed:0 / 255.0 green:0 / 255.0 blue:0 / 255.0 alpha:0.2]];
}

-(void)updateSelectionBySection:(int)section withTitle:(NSString *)title{
    //The value of section is from 0 to 4. 0 is News and 4 is About
    //But here 0 is about and 4 is News, so update the value
    section = abs(section - 4);
    
    //Just open the ViewController, No Selection
    UIButton *button = self.tabBarButtons[section];
    [button sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    if (title.length == 0) {

    }
    else
    {
        UINavigationController *navController = self.viewControllers[section];
        
        IndexFileOpenerViewController *detailsViewController = navController.viewControllers[0];
        
        [detailsViewController performSelector:@selector(openItemWithTitle:) withObject:title afterDelay:0.1];
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
