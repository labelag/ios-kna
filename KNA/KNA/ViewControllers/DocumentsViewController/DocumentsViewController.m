//
//  DocumentsViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "DocumentsViewController.h"

@interface DocumentsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *documentsTableView;

@property (nonatomic, strong) NSArray *documentsArr;
@property (nonatomic, strong) NSArray *constitutionArr;
@property (nonatomic, strong) NSArray *memoArr;

@end

@implementation DocumentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.documentsTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboutTableBg.png"]];
    DataManager *manager = [[DataManager alloc] init];
    self.documentsArr = [manager getDataOfSection:DOCUMENTS_SECTION];
    self.constitutionArr = [manager getDataOfSection:CONSTITUTION_SECTION];
    self.memoArr = [manager getDataOfSection:MEMO_SECTION];
    
    NSMutableArray *allItems = [NSMutableArray array];
    [allItems addObjectsFromArray:self.documentsArr];
    [allItems addObjectsFromArray:self.constitutionArr];
    [allItems addObjectsFromArray:self.memoArr];
    
    
    self.titles = allItems;
}


#pragma mark - UITableViewDataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titles.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= self.documentsArr.count) {
        return 0.0;
    }
    return 48.0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AboutAssemblyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    IndexType *type = self.titles[indexPath.row];
    cell.cellLabel.text = type.title;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self openIndexFileAtIndex:indexPath.row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
