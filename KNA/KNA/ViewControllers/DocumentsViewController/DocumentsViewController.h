//
//  DocumentsViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "DataManager.h"
#import "AboutAssemblyCell.h"
#import "IndexFileOpenerViewController.h"
@interface DocumentsViewController : IndexFileOpenerViewController <UITableViewDelegate, UITableViewDataSource>

@end
