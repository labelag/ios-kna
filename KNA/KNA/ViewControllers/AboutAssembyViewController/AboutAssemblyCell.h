//
//  AboutAssemblyCell.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
@interface AboutAssemblyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;

@end
