//
//  AboutAssembyViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/20/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "AboutAssemblyCell.h"
#import "DataManager.h"
#import "IndexFileOpenerViewController.h"
#import "StaticPageViewController.h"
@interface AboutAssembyViewController : IndexFileOpenerViewController <UITableViewDelegate, UITableViewDataSource, MWPhotoBrowserDelegate>

{
    int currentIndex;
}
@end
