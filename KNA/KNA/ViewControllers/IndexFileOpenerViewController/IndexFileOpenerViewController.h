//
//  IndexFileOpenerViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 4/3/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractDetailsViewController.h"
#import "IADownloadManager.h"
#import "IACacheManager.h"
#import "PDFViewerViewController.h"
#import "MBProgressHUD.h"
#import "StaticPageViewController.h"
#import "KAProgressLabel.h"
@interface IndexFileOpenerViewController : AbstractDetailsViewController<IADownloadManagerDelegate>

{
     MBProgressHUD *progressHud;
     KAProgressLabel *progressLabel;
}


-(void)downloadAndOpenFileWithUrl:(NSString *)url;
-(void)openIndexFile:(IndexType *)indexFile;
-(void)openIndexFileAtIndex:(int)row;
@end
