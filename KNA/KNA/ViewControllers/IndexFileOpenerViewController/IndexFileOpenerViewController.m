//
//  IndexFileOpenerViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 4/3/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "IndexFileOpenerViewController.h"
#import "MembersGroupViewController.h"


@interface IndexFileOpenerViewController ()


@property(nonatomic, strong) NSURL *currentURL;


@end

@implementation IndexFileOpenerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


-(void)openItemWithTitle:(NSString *)title
{
    int i = 0;
    for(IndexType *indexType in self.titles)
    {
        if([indexType.title isEqualToString:title])
        {
            [self openIndexFile:indexType];
            break;
        }
        i++;
    }
}

-(void)downloadAndOpenFileWithUrl:(NSString *)url
{
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [IADownloadManager downloadItemWithURL:[NSURL URLWithString:url] useCache:NO];
    [IADownloadManager attachListener:self toURL:[NSURL URLWithString:url]];

    
    [self setupProgressLabel];

}


-(void)setupProgressLabel
{
    if(progressLabel)
        [progressLabel removeFromSuperview];
    
    CGRect frame = self.view.frame;
    CGSize size = CGSizeMake(100, 100);
    progressLabel = [[KAProgressLabel alloc] initWithFrame:CGRectMake(frame.size.width / 2 - size.width / 2, frame.size.height / 2  - size.height  / 2, size.width, size.height)];

    UIColor *textColor = [UIColor colorWithRed:71.0 / 255.0 green:77.0 / 255.0 blue:93.0 / 255.0 alpha:1];
    UIColor *trackColor = [UIColor colorWithRed:241.0 / 255.0 green:241.0 / 255.0 blue:241.0 / 255.0 alpha:1];
    UIColor *progressColor =  [UIColor colorWithRed:48.0 / 255.0 green:198.0 / 255.0 blue:204.0 / 255.0 alpha:1];
    
    
    progressLabel.textColor = textColor;
    progressLabel.textAlignment = NSTextAlignmentCenter;
    progressLabel.font = [UIFont fontWithName:NADEEM_FONT size:23];
    [progressLabel setBorderWidth: 15.0];
    progressLabel.progressLabelVCBlock = ^(KAProgressLabel *label, CGFloat progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            int p = progress * 100;
            if (p >= 0) {
                [label setText:[NSString stringWithFormat:@"%d%%", p]];
            }
            
        });
    };
    [progressLabel setColorTable: @{
                                    NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):trackColor,
                                    NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):progressColor
                                    }];
    [self.view addSubview:progressLabel];
}



- (void) downloadManagerDidProgress:(float)progress
{
    [progressLabel setProgress:progress];
    NSLog(@"progress %f", progress);
}
- (void) downloadManagerDidFinish:(BOOL)success response:(id)response
{
    [progressLabel removeFromSuperview];
    progressLabel = nil;
    NSString* destinationFilePath = [self getTempPDFDirectory];
    NSError * error = nil;
    BOOL saveSuccess = [response writeToFile:destinationFilePath options:NSDataWritingAtomic error:&error];
    if (success && saveSuccess) {
        self.currentURL = [NSURL fileURLWithPath:destinationFilePath];
        [self openPDFViewControllerWithCurrentURL];
    }
}

-(void)openPDFViewControllerWithCurrentURL
{
    PDFViewerViewController *pdfViewerVC = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_PDF_VIEWER_VC];
    pdfViewerVC.masterDetailViewController = self.masterDetailViewController;
    pdfViewerVC.fileURL = self.currentURL;
    [self.navigationController pushViewController:pdfViewerVC animated:YES];
}

-(NSString *)getTempPDFDirectory
{
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* destinationFilePath = [NSString stringWithFormat: @"%@/cv.pdf", documentsDirectory];
    return destinationFilePath;
}

-(void)openStaticViewControllerWithFile:(IndexType *)indexFile
{
    StaticPageViewController *staticPageVC = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_STATIC_PAGE_VC];;
    staticPageVC.masterDetailViewController  = self.masterDetailViewController;
    staticPageVC.selectedIndex = indexFile;
    [self.navigationController pushViewController:staticPageVC animated:YES];
    
}

-(void)openMembersGroupWithFile:(IndexType *)indexFile
{
    MembersGroupViewController *membersGroupVC = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_MEMBERS_GROUP_VC];
    
    membersGroupVC.selectedIndexData = indexFile;
    membersGroupVC.masterDetailViewController = self.masterDetailViewController;
    [self.navigationController pushViewController:membersGroupVC animated:YES];
}

-(void)openIndexFile:(IndexType *)indexFile
{
    if ([indexFile.type isEqualToString:STATIC_TYPE]) {
        [self openStaticViewControllerWithFile:indexFile];
        
    }
    else if ([indexFile.type isEqualToString:DOCUMENT_TYPE])
    {
        //will be pdf
        [self downloadAndOpenFileWithUrl:indexFile.htmlText];
    }
    else if ([indexFile.type isEqualToString:MEMBERS_LIST_TYPE])
    {
        [self openMembersGroupWithFile:indexFile];
    }
    else if ([indexFile.type isEqualToString:URL_TYPE])
    {
        [self openExternalLinkInSafari:indexFile.url];
    }
}

-(void)openExternalLinkInSafari:(NSString *)link
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}

-(void)openIndexFileAtIndex:(int)row
{
    [self openIndexFile:self.titles[row]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
