//
//  MemberCell.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/8/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Member.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "Constants.h"
@interface MemberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *memberImageView;
@property (weak, nonatomic) IBOutlet UILabel *memberNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *accessoryIndicatorArrow;

-(void)bindMemeberData:(Member *)member;
@end
