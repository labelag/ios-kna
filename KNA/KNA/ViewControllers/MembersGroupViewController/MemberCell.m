//
//  MemberCell.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/8/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "MemberCell.h"

@implementation MemberCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)bindMemeberData:(Member *)member
{
    if (member.cv.length > 0) {
        self.accessoryIndicatorArrow.hidden = NO;
    }
    else
    {
        self.accessoryIndicatorArrow.hidden = YES;
    }
    self.memberNameLabel.text = member.name;
    self.memberNameLabel.font = [UIFont fontWithName:NADEEM_FONT size:18];
    self.titleLabel.font = [UIFont fontWithName:NADEEM_FONT size:12];
    self.titleLabel.text = member.title;
    [self.memberImageView setImageWithURL:[NSURL URLWithString:member.photoUrl] placeholderImage:[UIImage imageNamed:@"default-person.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
