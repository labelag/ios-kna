//
//  MembersGroupViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/8/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "MembersGroupViewController.h"

@interface MembersGroupViewController ()
@property (weak, nonatomic) IBOutlet UILabel *groupTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *membersTableView;
@property (nonatomic, strong) NSArray *members;
@end

@implementation MembersGroupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSArray *members = [self.selectedIndexData.members allObjects];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"memberID" ascending:YES];
    
    self.members = [members sortedArrayUsingDescriptors:@[sort]];
    [self updateHeaderView];
}

-(void)assignNavBarViews
{
    [super assignNavBarViews];
    [self addDefaultBackButton];
}

-(void)updateHeaderView
{
    self.groupTitleLabel.font = [UIFont fontWithName:GEDINAR_FONT_MEDIUM size:15];
    self.groupTitleLabel.text = self.selectedIndexData.title;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.members.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MemberCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell bindMemeberData:self.members[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Member *member = [self.members objectAtIndex:indexPath.row];
    
    //Do the following only if we have a cv
    if (member.cv.length > 0) {
        
        [self downloadAndOpenFileWithUrl:member.cv];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
