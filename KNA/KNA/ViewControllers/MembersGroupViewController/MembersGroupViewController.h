//
//  MembersGroupViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/8/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "IndexFileOpenerViewController.h"
#import "IndexType.h"
#import "ImageFile.h"
#import "Member.h"
#import "MemberCell.h"
#import "IADownloadManager.h"
#import "IACacheManager.h"
#import "PDFViewerViewController.h"
@interface MembersGroupViewController : IndexFileOpenerViewController <UITableViewDataSource, UITableViewDelegate>

{
}

@property (nonatomic, strong) IndexType *selectedIndexData;
@end
