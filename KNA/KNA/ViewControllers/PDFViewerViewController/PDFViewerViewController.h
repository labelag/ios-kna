//
//  PDFViewerViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 3/12/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDFViewerViewController : AbstractViewController


@property (nonatomic, strong) NSURL *fileURL;
@end
