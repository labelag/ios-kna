//
//  PDFViewerViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 3/12/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "PDFViewerViewController.h"

@interface PDFViewerViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PDFViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.fileURL];
    [self.webView loadRequest:request];
}

-(void)assignNavBarViews
{
    [super assignNavBarViews];
    [self addDefaultBackButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
