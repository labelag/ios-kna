//
//  NewsDetailsViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/25/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "AbstractViewController.h"
#import "NewsItem.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

@interface NewsDetailsViewController : AbstractViewController


@property (nonatomic, strong) NewsItem *selectedItem;
@end
