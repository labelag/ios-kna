//
//  PageContentViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/26/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "PageContentViewController.h"
#import "Constants.h"
@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    [self.backgroundImageView setImageWithURL:[NSURL URLWithString:self.imageFile] placeholderImage:[UIImage imageNamed:@"defaultHorizontalImg.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    
    self.titleLabel.text = self.titleText;

    self.titleLabel.font = [UIFont fontWithName:GEDINAR_FONT_LIGHT size:15];
    
}
- (IBAction)didTapOnPriorityNews:(UITapGestureRecognizer *)sender {
    NSLog(@"clicked on news");
    [self.delegate didClickedPageAtIndex:self.pageIndex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
