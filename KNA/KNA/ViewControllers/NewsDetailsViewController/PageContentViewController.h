//
//  PageContentViewController.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/26/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

@protocol PageContentViewControllerDelegate <NSObject>

-(void)didClickedPageAtIndex:(int)index;

@end

@interface PageContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) id<PageContentViewControllerDelegate> delegate;


@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end
