//
//  NewsDetailsViewController.m
//  KNA
//
//  Created by Hossam Ghareeb on 2/25/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#import "NewsDetailsViewController.h"
#import "NSString+HTML.h"
@interface NewsDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@end

@implementation NewsDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //set the pubdate
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE, dd MMM, yyyy";
    
    self.dateLabel.text = [dateFormatter stringFromDate:self.selectedItem.pubDate];
    //set the news image
    [self.newsImageView setImageWithURL:[NSURL URLWithString:self.selectedItem.horizontalImageUrl] placeholderImage:[UIImage imageNamed:@"defaultHorizontalImg.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    
    NSString *desc = self.selectedItem.newsDescription;
    desc = [desc stringByConvertingHTMLToPlainText];
    
    //Add the description in mutable string
//    NSMutableString *mutableDesc = [NSMutableString stringWithString:self.selectedItem.newsDescription];
//    
//    //Remove the image tag from string
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<img[^>]*>"
//                                                                           options:NSRegularExpressionCaseInsensitive
//                                                                             error:nil];
//    
//    [regex replaceMatchesInString:mutableDesc
//                          options:0
//                            range:NSMakeRange(0, mutableDesc.length)
//                     withTemplate:@""];
//    
//    //Remove html tags
//    NSString *newsDesc = [self stringByStrippingHTML:mutableDesc];
//    
//    
//    //Finally remove encoded html characters
//    
//    newsDesc = [newsDesc stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    
    //set title
    self.titleLabel.text = self.selectedItem.title;
    
//    self.titleLabel.font = [UIFont fontWithName:GEDINAR_FONT_MEDIUM size:16];
    CGSize size = [self.titleLabel sizeThatFits:CGSizeMake(self.titleLabel.frame.size.width, INT16_MAX)];
    CGRect frame = self.titleLabel.frame;
    frame.size.height = size.height;
    self.titleLabel.frame = frame;
    
    
    frame = self.descriptionTextView.frame;
    frame.origin.y = self.titleLabel.frame.origin.y + size.height + 13;
    
    frame.size.height -= (size.height + 13);
    self.descriptionTextView.frame = frame;
    
    //set desc
    self.descriptionTextView.text = desc;
//    self.descriptionTextView.font = [UIFont fontWithName:GEDINAR_FONT_LIGHT size:16];
}

-(NSString *) stringByStrippingHTML:(NSString *)string {
    NSRange r;
    NSString *s = [string copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(void)assignNavBarViews
{
    [super assignNavBarViews];
    [self addDefaultBackButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
