//
//  Constants.h
//  KNA
//
//  Created by Hossam Ghareeb on 2/16/14.
//  Copyright (c) 2014 Hossam Ghareeb. All rights reserved.
//

#ifndef KNA_Constants_h
#define KNA_Constants_h

#define INDEX_FILE_NOTIFICATION @"indexFileNotification"
#define INDEX_FILE_RESPONSE_KEY @"RES_KEY"

#pragma mark - API Requests -

#define NEWS_RSS_URL @"http://www.kna.kw/clt/rss-mobile-app-request-services-2014.asp"
#define DATA_URL @"http://www.kna.kw/app/index.json"

#pragma mark - Fonts -
#define GEDINAR_FONT_LIGHT @"GEDinarOne-Light"
#define GEDINAR_FONT_LIGHT_ITALIC @"GEDinarOne-LightItalic"
#define GEDINAR_FONT_MEDIUM @"GEDinarOne-Medium"
#define GEDINAR_FONT_MEDIUM_ITALIC @"GEDinarOne-MediumItalic"
#define NADEEM_FONT @"Nadeem"

#pragma mark - Sections -
#define MEMO_SECTION @"memo"
#define CONSTITUTION_SECTION @"constitution"
#define MEMBERS_SECTION @"members"
#define DOCUMENTS_SECTION @"documents"
#define ALBUMS_SECTION @"albums"
#define ABOUT_SECTION @"about"


#pragma mark - Types -
#define DOCUMENT_TYPE @"document"
#define STATIC_TYPE @"static"
#define PDF_MIME_TYPE @"application/pdf"
#define URL_TYPE @"url"
#define MEMBERS_LIST_TYPE @"members-list"

#pragma mark - StoryBoard -

#define STORYBOARD_ID_TABBAR_VC @"tabbarVC"
#define STORYBOARD_ID_RIGHT_MENU @"RightMenu"
#define STORYBOARD_ID_NEWS_DETAILS @"newsDetails"
#define STORYBOARD_ID_PAGE_VC @"PageViewController"
#define STORYBOARD_ID_PAGE_CONTENT_VC @"PageContentVC"
#define STORYBOARD_ID_PHOTO_BROWSER @"photoBrowserVC"
#define STORYBOARD_ID_STATIC_PAGE_VC @"staticPageVC"
#define STORYBOARD_ID_PDF_VIEWER_VC @"pdfViewerVC"
#define STORYBOARD_ID_MEMBERS_GROUP_VC @"membersGroupVC"


#define SEGUE_OPEN_PHOTO_BROWSER @"openPhotoBrowser"
#define SEGUE_OPEN_MEMBERS_GROUP @"openMembersGroup"
#define SEGUE_OPEN_CV @"openCV"
#define SEGUE_OPEN_STATIC_VIEW @"openStaticView"

#endif
